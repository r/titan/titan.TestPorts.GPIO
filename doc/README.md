<!---
##############################################################################
# Copyright (c) 2000-2019 Ericsson Telecom AB
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
#
#  Contributors:
#   Lenard Nagy
#
##############################################################################
-->

# GPIO testport for use with Titan test executor on Raspberry Pi 

## General introduction
Titan is a TTCN-3 compiler and test executor, which is able to run on the Raspberry Pi (model 2B and 3B were tested). GPIO (general purpose input/output) pins on the Raspberry Pi are able to control and/or receive signals from physical computing devices (leds, buttons, valves, etc...). Testports are programmable connections between Titan and any physical interface on a device, in this case GPIO pins on the Raspberry Pi.

The current implementation is using the sysfs interface to access GPIO pins.

## Usage

### Preparations

* In Your TTCN-3 module the GPIOPinPort module must be included:

    import from GPIOPinPort all;

* Create a component in the module:

** Recommended way: extend the GPIO_Base component in GPIOPinPort module where the whole GPIO is declared as a port array

    type component GPIO extends GPIO_Base { <Your code> }

** Custom solution: 
*** !!Important: the name of the port instance MUST be either gpio# or in case of port arrays gpio[#], where # is an integer between (and including) 2 and 27, which will be the actual pin number of the GPIO!

    type component GPIO {
	port GPIO_Pin_Port gpio27;
	port GPIO_Pin_Port gpio[2..26];
        timer t_short;
    }

* Map any or all the ports You want to use:

    map(self:gpio[11], system:gpio[11]);
    map(self:gpio27, system:gpio27);
    map(self:gpio[26], system:gpio[26]);

### Setting a pin to output, and setting the value of the pin

* To set direction on the gpio pin to OUT a "send" operation is to be performed:

    var GPIO_PIN_DIRECTION gpio27_direction := OUT;
    gpio27.send(gpio27_direction);

* Similarly, to set the value (HIGH/LOW) a "send" operation is to be performed:

    var GPIO_PIN_VALUE gpio27_value := HIGH;
    gpio27.send(gpio27_value);

### Listening for input changes on pin(s)
* Please note that without an actual physical circuit connected to the pins no relyable state can be read as the state of a pin is "undefined".

    var integer v_index;
    
    var GPIO_PIN_DIRECTION gpio_direction_in := IN;
    gpio[26].send(gpio_direction_in);
    gpio27.send(gpio_direction_in);
    
    t_short.start(5.0);
    	
    alt {
        [] gpio27.receive {
           setverdict(pass,"message received on port index 27");
           }
        [] any from gpio.receive -> @index value v_index {
           setverdict(fail,"unexpected message received on port index: ",v_index);
           }
        [] t_short.timeout {
           log("Timeout while waiting for input to change");
           setverdict(inconc);
           }
    }	

### Unmap all the used ports
    
    unmap(self:gpio27, system:gpio27);
    unmap(self:gpio[11], system:gpio[11]);
    unmap(self:gpio[26], system:gpio[26]);

## References
* About Titan: https://projects.eclipse.org/projects/tools.titan
* About TTCN-3: http://www.ttcn-3.org/
* About Raspberry Pi: https://www.raspberrypi.org/
* About GPIO on Raspberry Pi: https://www.raspberrypi.org/documentation/usage/gpio/README.md

