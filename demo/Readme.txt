###############################################################################
## Copyright (c) 2000-2019 Ericsson Telecom AB
## All rights reserved. This program and the accompanying materials
## are made available under the terms of the Eclipse Public License v2.0
## which accompanies this distribution, and is available at
## http:##www.eclipse.org#legal#epl-v10.html
##
## Contributors:
##   Gyorgy Rethy
##  version R2A
###############################################################################

The GPIO demo has dependence of (is using) the TCCFileIO_Functions.ttcn and
TCCFileIO.cc files from the project TCC Useful Functions.
What to do:
1. Get TCC Useful Functions:
   git clone https://github.com/eclipse/titan.Libraries.TCCUsefulFunctions.git

2a. Create softlinks to the files TCCFileIO_Functions.ttcn and TCCFileIO.cc
    into your build directory
   
3a. Add to your makefile:
    to TTCN3_MODULES add TCCFileIO_Functions.ttcn
    to GENERATED_SOURCES add TCCFileIO_Functions.cc
    to GENERATED_HEADERS add TCCFileIO_Functions.hh
    to USER_SOURCES add TCCFileIO.cc
    to GENERATED_OBJECTS add TCCFileIO_Functions.o
    to USER_OBJECTS add TCCFileIO.o

2b. Or if using Eclipse designer, simply create a project for
    TCC_Useful_Functions and add it as project dependency to the GPIO demo
    project
    [Project/Properties/Project References and check the checkbox of your
     TCC_Useful_Functions project]

The GPIO test port has one runtime parameter:
	debug
		"YES"	detailed debug logging is enabled, test port logs
			appear in Titan log file as DEBUG category events,
			starting with "GPIO Test Port (<gpio port name>):"
		"NO"	debug logging is disabled; use the port in this mode,
			unless you are developing it or experiencing hard to
			understand behaviour